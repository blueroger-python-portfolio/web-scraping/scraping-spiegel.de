Scrapy Learning project - Scraping Spiegel.de
===================

Learning project - Web Scraping with Scrapy


## Authors

* **Mihai Palaghian** - [Blueroger](https://www.blueroger.de)

## License

No License - This project is only a project reference. Commercial or private usage not permitted.
