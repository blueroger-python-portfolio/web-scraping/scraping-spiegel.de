# -*- coding: utf-8 -*-
import scrapy
import datetime
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Join
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from urllib.parse import urlsplit
import logging

from spiegelSpider.items import SpiegelArticle


class ArticleSpider(CrawlSpider):
    name = 'article'
    allowed_domains = ['spiegel.de']
    start_urls = ['https://www.spiegel.de/politik/']

    rules = (
        Rule(LinkExtractor(restrict_xpaths='//*[contains(@class,"link-right")]')),
        Rule(LinkExtractor(restrict_xpaths='//*[@class="article-title "]',
                           deny='^(https://www.spiegel.de/video/)'),
             callback='parse_item')
        )

    def parse_item(self, response):
        """
        @url https://www.spiegel.de/politik/
        @returns items 1
        @ scrapes title articleIntro articleAuthor articleDate
        @scrapes articleCategorie articleSubcategorie articleText
        """
        il = ItemLoader(item=SpiegelArticle(), response=response)
        il.add_xpath('articleTitle', '//h2[contains(@class, "article-title")]'
                     '//span[@class="headline"]/text()',
                     MapCompose(str.strip))

        if response.css('.article-intro'):
            il.add_xpath('articleIntro',
                         '//*[@class="article-intro"]//text()',
                         MapCompose(str.strip, str.title))
        else:
            il.add_value('articleIntro', '')

        if response.css('.author'):
            il.add_xpath('articleAuthor', '//*[@rel="author"]/text()',
                         MapCompose(str.strip))
        else:
            il.add_value('articleAuthor', '')
        articleDate = '//div[@data-sponlytics-area="box-article"]' \
                      '//*[@class="article-function-date"]//@datetime'

        if response.xpath(articleDate):
            il.add_xpath('articleDate', articleDate)
        else:
            il.add_value('articleDate', '')

        # Find article category and subcategory from url
        # path: /politik/deutschland/spiegel-reihe-wir-...-a-1276832.html
        url = urlsplit(response.url)
        path = url.path
        urlkeys = path.split("/")
        if urlkeys[1]:
            il.add_value('articleCategory', urlkeys[1])
        else:
            il.add_value('articleCategory', '')
        if urlkeys[2]:
            il.add_value('articleSubcategory', urlkeys[2])
        else:
            il.add_value('articleSubcategory', '')

        il.add_xpath('articleText',
                     '//*[contains(@class,"article-section")]//p/text()',
                     MapCompose(str.strip), Join())

        il.add_value('articleUrl', response.url)
        il.add_value('project', self.settings.get('BOT_NAME'))
        il.add_value('spider', self.name)
        il.add_value('spiderDate', datetime.datetime.now())

        # Get Article comments
        if response.xpath('//div[@id="js-article-comments-box-pager"]'):

            # Building the URL's from comments pagination Infos
            # https://www.spiegel.de/fragments/community/spon-937042-1.html
            # https://www.spiegel.de/fragments/community/spon-937042-6.html
            # Base URL from to build
            moreCommentsUrl = 'https://www.spiegel.de/fragments/community/spon-'

            # Artikel/Thread id
            # only working if comments are permitted
            threadid = response.xpath('//div[@id="js-article-comments-box-form"]//'
                                      'input[@name="threadid"]/@value').get()
            # Number of Comments  as string
            # string: insgesamt 63 Beiträge
            CountString = response.xpath('//div[contains(@class, '
                                         '"article-comments-box")]'
                                         '/span/text()').get().strip()

            # extracting number of comments as int
            commenstCount = [int(i) for i in CountString.split() if i.isdigit()]

            # nur artikel mit nicht mehr als 50 Kommentare
            # only article with less than 50 comments
            # der ItemLoader wird an der nächste parse Funktion weitergegeben
            if commenstCount[0] < 50:
                for n in range(1, commenstCount[0], 5):
                    url = moreCommentsUrl + threadid + "-" + str(n) + ".html"
                    yield scrapy.Request(url, callback=self.parse_comments,
                                         meta={"item": il.load_item()},
                                         dont_filter=True)

    def parse_comments(self, response):
        loader = ItemLoader(item=response.meta['item'], response=response)

        commentsList = []
        getComments = response.xpath('//div[@class="article-comment"]')
        for comment in getComments:
            commentComp = {}
            commentTitle = comment.xpath('.//*[@class="article-comment-title"]'
                                         '/a/text()').get()
            commentText = comment.xpath('.//*[@class="js-article-post-full-text'
                                        '"]/text()').get()
            commentComp["comment"] = commentTitle.strip() + ' ' + commentText.strip()
            commentAuthor = comment.xpath('.//*[@class="article-comment-user"]/a//text()').get()
            commentComp["comment_author"] = commentAuthor
            commentsList.append(commentComp)

        loader.add_value('articleComments', commentsList)
        return loader.load_item()
