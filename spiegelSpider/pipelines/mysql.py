import traceback

from pymysql import OperationalError
from pymysql.cursors import DictCursor

from twisted.internet import defer
from twisted.enterprise import adbapi

from scrapy.exceptions import NotConfigured

import db.db_models as db


class MysqlWriter(object):

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)

    def __init__(self, crawler):

        # Report connection error only once
        self.report_connection_error = True

        dbparams = dict(
            host=crawler.settings.get('MAYSQL_HOST', None),
            port=crawler.settings.get('MYSQL_PORT', None),
            user=crawler.settings.get('MYSQL_USER', None),
            passwd=crawler.settings.get('MYSQL_PASSWD', None),
            db=crawler.settings.get('MYSQL_DB', None),
            use_unicode=True,
            charset='utf8',
            connect_timeout=50,
            cursorclass=DictCursor
            )

        dbparams = dict((k, v) for k, v in dbparams.items() if v)

        if not dbparams:
            raise NotConfigured

        self.dbpool = adbapi.ConnectionPool("pymysql", **dbparams)

    def close_spider(self, spider):
        self.dbpool.close()

    @defer.inlineCallbacks
    def process_item(self, item, spider):
        logger = spider.logger

        try:
            yield self.dbpool.runInteraction(self.do_insert, item)
            logger.info('Conneted to Database.')
        except OperationalError:
            if self.report_connection_error:
                logger.error("Can't connect to MySQL:")
                self.report_connection_error = False
        except:
            print(traceback.format_exc())

        defer.returnValue(item)

    @staticmethod
    def do_insert(cursor, item):

        # Insert article
        db.insert_article(cursor, item)
        # Get article Id
        articleId = db.get_article_id(cursor, item["articleUrl"])
        # Insert authors
        db.insert_author(cursor, item, articleId)
        # Insert article comments
        db.insert_comments(cursor, item, articleId)
        # Insert article category and subcategory
        db.insert_category(cursor, item, articleId)
