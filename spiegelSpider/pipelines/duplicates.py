# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


class DuplicatesPipeline(object):

    def __init__(self):
        self.urls_seen = []

    def process_item(self, item, spider):
        if item['articleUrl'] in self.urls_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.urls_seen.append(item['articleUrl'])
            return item
