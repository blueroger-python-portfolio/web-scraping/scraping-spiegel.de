# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class SpiegelArticle(Item):
    # Primary Items
    articleUrl = Field()
    articleTitle = Field()
    articleIntro = Field()
    articleDate = Field()
    articleText = Field()
    articleCategory = Field()
    articleSubcategory = Field()
    articleAuthor = Field()
    articleComments = Field()
    commentAuthor = Field()

    # Calculated Fields
    images = Field()

    # Internal Fields
    project = Field()
    spider = Field()
    server = Field()
    spiderDate = Field()
