# https://stackoverflow.com/questions/6323860/sibling-package-imports
# Import module with setuptools
# Install package in root dir: pip install -e .

from setuptools import setup, find_packages

setup(name='spiegelSpider',
      version='1.0',
      packages=find_packages(),
      entry_points={'scrapy': ['settings = spiegelSpider.settings']},)
