# -*- coding: utf-8 -*-
import pymysql.cursors
from pymysql.cursors import DictCursor
import json
import csv


def db_connect():

    try:
        connection = pymysql.connect(host='localhost',
                                     user='root',
                                     password='',
                                     db='spiegelde',
                                     charset='utf8',
                                     cursorclass=DictCursor)
        if connection:
            print("Connected")

    except pymysql.OperationalError:
        print("No Connection")

    return connection


def get_article(cursor):
    """ SQL Query - get article from database """

    sql = """SELECT a.article_id as articleId, a.article_title as articleTitle,
         a.article_url as articleUrl,
        DATE_FORMAT(a.article_date, '%Y-%m-%d %T') AS articleDate,
        a.article_intro as articleIntro,
        a.article_text as articleText,
        au.article_author_name as articleAuthor,
        cm.comment_text as articleComments,
        cm.comment_author_name as commentAuthor,
        cat.category_name as articleCategory,
        cats.category_name as articleSubcategory
        FROM article a
        LEFT JOIN article_author aa ON article_id=aa.fk_article_id
        LEFT JOIN author au ON aa.fk_author_id=au.article_author_id
        LEFT JOIN (SELECT fk_article_id, comment_text, comment_author_name
        FROM comments c2
        LEFT JOIN comment_author ca
        ON c2.fk_comment_author_id=ca.comment_author_id) cm
        ON a.article_id=cm.fk_article_id
        LEFT JOIN (SELECT article_id, category_id, category_name
        FROM article a
        LEFT JOIN article_category ac ON a.article_id=ac.fk_article_id
        LEFT JOIN category ct ON ac.fk_category_id=ct.category_id
        WHERE ct.fk_category_id IS NULL)
        cat ON a.article_id=cat.article_id
        LEFT JOIN (SELECT article_id, category_id, category_name
        FROM article a
        LEFT JOIN article_category ac ON a.article_id=ac.fk_article_id
        LEFT JOIN category ct ON ac.fk_category_id=ct.category_id
        WHERE ct.fk_category_id IS NOT NULL)
        cats ON a.article_id=cats.article_id"""

    cursor.execute(sql)
    rows = cursor.fetchall()

    return rows


def write_json(rows):
    """ write json file """

    try:
        with open('../db_output.json', "w+", encoding='utf-8') as json_file:
            json.dump(rows, json_file, default=str)
    except:
        print("Error")


def write_csv(rows):
    """ write csv file """

    csv_columns = ["articleId", "articleTitle", "articleUrl",
                   "articleDate", "articleIntro",
                   "articleText",
                   "articleAuthor", "articleComments",
                   "commentAuthor", "articleCategory",
                   "articleSubcategory"]

    try:
        with open('../db_output.csv', "w+", encoding="utf-8") as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=csv_columns)
            writer.writeheader()

            for row in rows:
                writer.writerow(row)

    except IOError:
        print("I/O error")


def db_query():
    try:
        connection = db_connect()

        with connection.cursor() as cursor:

            db_article = get_article(cursor)

            write_csv(db_article)
            # write_json(db_article)

    finally:
        connection.close()
        print("Connection closed")


if __name__ == '__main__':
    db_query()
