import traceback
import pymysql.cursors
from pymysql.cursors import DictCursor
import json


def connect():
    """ Connect to MySQL database """
    try:

        conn = pymysql.connect(host='localhost',
                               user='root',
                               password='',
                               db='spiegelde',
                               charset='utf8',
                               cursorclass=DictCursor)
        if conn:
            print("Connected to Database")

    except pymysql.OperationalError:
        print("No connection")

    except:
        print(traceback.format_exc())

    return conn


def get_article_id(cursor, articleUrl):
    """ Get the current article id """

    args = (articleUrl)
    sql = """SELECT article_id FROM article
             WHERE article_url=%s LIMIT 1"""

    cursor.execute(sql, args)

    return cursor.fetchone()


def insert_article(cursor, item):
    """
    Insert article
    article_url is unique
    to avoid duplicate article entry
    """
    article = (
        item["articleUrl"],
        item["articleTitle"],
        item["articleDate"],
        item["articleIntro"],
        item["articleText"]
        )

    # Insert article
    sql = """INSERT IGNORE INTO article (article_url,
    article_title, article_date, article_intro, article_text)
    VALUES (%s,%s,%s,%s,%s)"""

    cursor.execute(sql, article)


def insert_author(cursor, item, articleId):
    """
    Insert author
    article_author_name column is unique
    to avoid duplicate entry
    """
    if item["articleAuthor"]:

        if item["articleAuthor"][0] is not "":

            articleAuthor = (item["articleAuthor"])
            sql = """INSERT IGNORE INTO author (article_author_name)
            VALUES (%s)"""

            cursor.execute(sql, articleAuthor)

            # Find author id
            articleAuthorId = (item["articleAuthor"][0])
            sql = """SELECT article_author_id FROM author
                    WHERE article_author_name=%s LIMIT 1"""

            cursor.execute(sql, articleAuthorId)
            authorId = cursor.fetchone()

            # Insert article, author relationship
            args = (articleId["article_id"],
                    authorId["article_author_id"])
            sql = """INSERT IGNORE INTO article_author (fk_article_id, fk_author_id)
             VALUES (%s,%s)"""

            cursor.execute(sql, args)


def insert_comments(cursor, item, articleId):
    """
    Insert comments from article
    """
    if item["articleComments"]:

        # Delete all previous comments

        if articleId is not None:
            sql = "DELETE FROM comments WHERE fk_article_id=" + str(articleId["article_id"])
            cursor.execute(sql)

        # Comments
        for comment in item["articleComments"]:

            # Insert comment author
            commentAuthor = comment["comment_author"]
            commentAuthor = pymysql.escape_string(comment["comment_author"])

            sql = """INSERT IGNORE INTO comment_author
                (comment_author_name) VALUES (%s)"""

            cursor.execute(sql, commentAuthor)

            # Insert comments
            commentText = pymysql.escape_string(comment["comment"])

            sql = """SELECT comment_author_id FROM comment_author
                                WHERE comment_author_name=%s LIMIT 1"""

            cursor.execute(sql, commentAuthor)
            fkCommentAuthorId = cursor.fetchone()

            args = (commentText,
                    articleId["article_id"],
                    fkCommentAuthorId["comment_author_id"])
            sql = """INSERT IGNORE INTO comments (comment_text, fk_article_id,
                    fk_comment_author_id) VALUES (%s,%s,%s)"""

            cursor.execute(sql, args)


def insert_category(cursor, item, articleId):
    """ Insert Article Category """

    if item["articleCategory"]:

        if item["articleCategory"] is not '':

            articleCategory = (item["articleCategory"][0])
            sql = """INSERT IGNORE INTO category (category_name) VALUES(%s)"""

            cursor.execute(sql, articleCategory)

            # Find Category Id
            sql = """SELECT category_id FROM category
                     WHERE category_name=%s LIMIT 1"""

            cursor.execute(sql, articleCategory)
            categoryId = cursor.fetchone()

            # Insert article, category relationship
            args = (articleId["article_id"],
                    categoryId["category_id"])
            sql = """INSERT IGNORE INTO article_category (fk_article_id, fk_category_id)
                     VALUES (%s,%s)"""

            cursor.execute(sql, args)

        # Insert Article Subcategory
        if item["articleSubcategory"]:

            if item["articleSubcategory"] is not '':

                args = (
                    item["articleSubcategory"][0],
                    categoryId["category_id"]
                    )

                sql = """INSERT IGNORE INTO category
                (category_name, fk_category_id)
                VALUES(%s,%s)"""

                cursor.execute(sql, args)

            # Find Category Id
            articleSubcategory = (item["articleSubcategory"][0])
            sql = """SELECT category_id FROM category
                     WHERE category_name=%s LIMIT 1"""
            cursor.execute(sql, articleSubcategory)
            subCategoryId = cursor.fetchone()

            # Insert article, subcategory relationship
            args = (articleId["article_id"],
                    subCategoryId["category_id"])
            sql = """INSERT IGNORE INTO article_category (fk_article_id, fk_category_id)
                     VALUES (%s,%s)"""
            cursor.execute(sql, args)


def insert_all():
    try:
        with open('../article.json', encoding='utf-8') as json_file:
            data = json.load(json_file)

        connection = connect()
        with connection.cursor() as cursor:

            for item in data:

                # Insert article
                insert_article(cursor, item)
                # Get article Id
                articleId = get_article_id(cursor, item["articleUrl"])
                # Insert authors
                insert_author(cursor, item, articleId)
                # Insert article comments
                insert_comments(cursor, item, articleId)
                # Insert article category and subcategory
                insert_category(cursor, item, articleId)

        connection.commit()

    finally:
        connection.close()
        print("Connection closed")


if __name__ == '__main__':
    insert_all()
