CREATE DATABASE IF NOT EXISTS spiegelde DEFAULT CHARACTER SET utf8;

use spiegelde;

CREATE TABLE IF NOT EXISTS article (
	article_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	article_url VARCHAR(255) UNIQUE NOT NULL,
	article_title VARCHAR(255),
	article_date DATETIME,
	article_intro TEXT,
	article_text TEXT
);

CREATE TABLE IF NOT EXISTS author (
	article_author_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	article_author_name VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS article_author (
	fk_article_id INT NOT NULL,
	fk_author_id INT NOT NULL,
	PRIMARY KEY (fk_article_id, fk_author_id),
	CONSTRAINT fk_article
		FOREIGN KEY (fk_article_id)
			REFERENCES article (article_id),
	CONSTRAINT fk_author
		FOREIGN KEY (fk_author_id)
			REFERENCES author (article_author_id)
);

CREATE TABLE IF NOT EXISTS comment_author (
	comment_author_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	comment_author_name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS comments (
	comment_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	comment_text TEXT NOT NULL,
	fk_article_id INT NOT NULL,
	fk_comment_author_id INT NOT NULL,
	CONSTRAINT fk_comment_article
		FOREIGN KEY (fk_article_id)
			REFERENCES article (article_id)
			ON UPDATE CASCADE
			ON DELETE CASCADE,
	CONSTRAINT fk_comment_author
		FOREIGN KEY (fk_comment_author_id)
			REFERENCES comment_author (comment_author_id)
			ON UPDATE CASCADE
			ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS category (
	category_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
	category_name VARCHAR(100) NOT NULL,
	fk_category_id INT,
	CONSTRAINT fk_category
		FOREIGN KEY (fk_category_id)
			REFERENCES category (category_id)
);
CREATE TABLE IF NOT EXISTS article_category (
	fk_article_id INT NOT NULL,
	fk_category_id INT NOT NULL,
	PRIMARY KEY (fk_article_id, fk_category_id),
	CONSTRAINT fk_article2
		FOREIGN KEY (fk_article_id)
		REFERENCES article (article_id),
	CONSTRAINT fk_category2
		FOREIGN KEY (fk_category_id)
			REFERENCES category (category_id)
);
